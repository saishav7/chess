package com.chess.model.pieces;

import com.chess.model.Board;
import com.chess.model.Piece;

/**
 * Created by saishav7 on 30/04/2016.
 */
public class Bishop extends Piece {

    public Bishop(boolean playerWhite, int x, int y) {
        super('B', playerWhite, x, y);
    }

    @Override
    public boolean isValid(int toX, int toY) {
        int fromX = getX();
        int fromY = getY();

        if(super.isValid(toX, toY) == false)
            return false;

        if(toX - fromX == toY - fromY)
            return true;

        return false;
    }

}
