package com.chess.model.pieces;

import com.chess.model.Board;
import com.chess.model.Piece;

/**
 * Created by saishav7 on 30/04/2016.
 */
public class Pawn extends Piece {

    public Pawn(boolean playerWhite, int x, int y) {
        super('P', playerWhite, x, y);
    }

    @Override
    public boolean isValid(int toX, int toY) {
        int fromX = getX();
        int fromY = getY();
        if (super.isValid(toX, toY) == false) {
            return false;
        }

        if (isPlayerWhite()) {
            if ((toX == fromX && toY == fromY + 1) || (toX == fromX + 1 && toY == fromY + 1)
                    || (toX == fromX - 1 && toY == fromY + 1)) {
                return true;
            }
        } else {
            if ((toX == fromX && toY == fromY - 1) || (toX == fromX + 1 && toY == fromY - 1)
            || (toX == fromX - 1 && toY == fromY - 1)) {
                return true;
            }
        }
        return false;
    }
}
