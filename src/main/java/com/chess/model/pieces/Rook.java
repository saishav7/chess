package com.chess.model.pieces;

import com.chess.model.Piece;

/**
 * Created by saishav7 on 30/04/2016.
 */
public class Rook extends Piece {

    public Rook(boolean playerWhite, int x, int y) {
        super('R', playerWhite, x, y);
    }

}
