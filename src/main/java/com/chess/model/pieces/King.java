package com.chess.model.pieces;

import com.chess.model.Piece;

/**
 * Created by saishav7 on 30/04/2016.
 */
public class King extends Piece {

    public King(boolean playerWhite, int x, int y) {
        super('K', playerWhite, x, y);
    }

}
