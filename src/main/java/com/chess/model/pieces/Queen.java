package com.chess.model.pieces;

import com.chess.model.Piece;

/**
 * Created by saishav7 on 30/04/2016.
 */
public class Queen extends Piece {

    public Queen(boolean playerWhite, int x, int y) {
        super('Q', playerWhite, x, y);
    }

}