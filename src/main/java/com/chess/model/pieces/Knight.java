package com.chess.model.pieces;

import com.chess.model.Board;
import com.chess.model.Piece;

/**
 * Created by saishav7 on 30/04/2016.
 */
public class Knight extends Piece {

    public Knight(boolean playerWhite, int x, int y) {
        super('N', playerWhite, x, y);
    }

    @Override
    public boolean isValid(int toX, int toY) {
        int fromX = getX();
        int fromY = getY();

        if(super.isValid(toX, toY) == false)
            return false;

        if((toX == fromX + 1 && toY == fromY + 2) || (toX == fromX + 1 && toY == fromY - 2) ||
                (toX == fromX - 1 && toY == fromY + 2) || (toX == fromX - 1 && toY == fromY - 2) ||
                (toX == fromX + 2 && toY == fromY + 1) || (toX == fromX + 2 && toY == fromY - 1) ||
                (toX == fromX - 2 && toY == fromY + 1) || (toX == fromX - 2 && toY == fromY - 1)) {
            return true;
        }

        return false;
    }

}
