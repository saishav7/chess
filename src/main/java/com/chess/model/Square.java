package com.chess.model;

/**
 * Created by saishav7 on 30/04/2016.
 */

public class Square {
    int x;
    int y;
    Piece piece;

    public Square(int x, int y) {
        super();
        this.x = x;
        this.y = y;
        piece = null;
    }

    public void occupySquare(Piece piece) {
        //place piece here
        this.piece = piece;
    }

    public boolean isOccupied() {
        if (piece != null)
            return true;
        return false;
    }

    public Piece getPiece() {
        return piece;
    }

}
