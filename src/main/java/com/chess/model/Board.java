package com.chess.model;

/**
 * Created by saishav7 on 30/04/2016.
 */
public class Board {

    private Square[][] squares = new Square[8][8];

    public Board() {
        super();
        for (int i = 0; i < squares.length; i++) {
            for (int j = 0; j < squares.length; j++) {
                squares[i][j] = new Square(i, j);
            }
        }
    }

    public Square getSquare(int x, int y) {
        return squares[x][y];
    }


    public void print() {
        for (int row = 7; row >= 0; row--) {
            System.out.println("");
            System.out.println(" ---------------------------------");
            System.out.print(row+1);
            for (int column = 0; column <= 7; column++) {
                System.out.print("| " + (null == squares[column][row].getPiece() ? "  " :
                         squares[column][row].getPiece().getName() + " "));
            }
            System.out.print("|");
        }
        System.out.println("");
        System.out.println(" ---------------------------------");
        System.out.println("   a   b   c   d   e   f   g   h  ");
    }
}
