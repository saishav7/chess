package com.chess.model;

/**
 * Created by saishav7 on 30/04/2016.
 */
public class Piece {
    private boolean available;
    private char name;
    private boolean playerWhite;
    private int x;
    private int y;

    public Piece(char name, boolean playerWhite, int x, int y) {
        super();
        this.name = name;
        this.playerWhite = playerWhite;
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public char getName() {
        return name;
    }

    public boolean isPlayerWhite() {
        return playerWhite;
    }

    public boolean isValid(int toX, int toY) {
        int fromX = getX();
        int fromY = getY();
        if (toX == fromX && toY == fromY)
            return false; //cannot move nothing
        if (toX < 0 || toX > 7 || fromX < 0 || fromX > 7 || toY < 0 || toY > 7 || fromY < 0 || fromY > 7)
            return false;
        return true;
    }

}
