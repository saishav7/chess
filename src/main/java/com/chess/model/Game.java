package com.chess.model;

/**
 * Created by saishav7 on 30/04/2016.
 */
public class Game {
    private Board board = new Board();
    private Player white;
    private Player black;

    public Game() {
        super();
    }

    public Board getBoard() {
        return board;
    }

    public void setBoard(Board board) {
        this.board = board;
    }

    public Player getWhite() {
        return white;
    }

    public void setWhite(Player white) {
        this.white = white;
    }

    public Player getBlack() {
        return black;
    }

    public void setBlack(Player black) {
        this.black = black;
    }

    public boolean initializeBoardGivenPlayers() {
        if(this.black == null || this.white == null)
            return false;
        this.board = new Board();
        for(int i=0; i<black.getPieces().size(); i++){
            board.getSquare(black.getPieces().get(i).getX(), black.getPieces().get(i).getY()).occupySquare(black.getPieces().get(i));
        }

        for(int i=0; i<white.getPieces().size(); i++){
            board.getSquare(white.getPieces().get(i).getX(), white.getPieces().get(i).getY()).occupySquare(white.getPieces().get(i));
        }
        return true;
    }

}
