package com.chess.model;

import com.chess.model.pieces.*;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by saishav7 on 30/04/2016.
 */
public class Player {

    //if false then colour is assumed black
    public boolean white;

    private List<Piece> pieces = new ArrayList<>();

    public Player(boolean white) {
        super();
        this.white = white;
    }

    public List<Piece> getPieces() {
        return pieces;
    }

    //standard chess game initialization
    public void initializePieces() {

        if(this.white == true){
            for(int i = 0; i < 8; i++){ // draw pawns
                pieces.add(new Pawn(true, (char) (i+97),2));
            }
            pieces.add(new Rook(true, 'a', 1));
            pieces.add(new Rook(true, 'h', 1));
            pieces.add(new Bishop(true, 'c', 1));
            pieces.add(new Bishop(true, 'f', 1));
            pieces.add(new Knight(true, 'b', 1));
            pieces.add(new Knight(true, 'g', 1));
            pieces.add(new Queen(true, 'd', 1));
            pieces.add(new King(true, 'e', 1));
        }
        else{
            for(int i = 0; i < 8; i++){ // draw pawns
                pieces.add(new Pawn(false, (char) (i+97),7));
            }
            pieces.add(new Rook(false, 'a', 8));
            pieces.add(new Rook(false, 'h', 8));
            pieces.add(new Bishop(false, 'c', 8));
            pieces.add(new Bishop(false, 'f', 8));
            pieces.add(new Knight(false, 'b', 8));
            pieces.add(new Knight(false, 'g', 8));
            pieces.add(new Queen(false, 'd', 8));
            pieces.add(new King(false, 'e', 8));
        }

    }
}
