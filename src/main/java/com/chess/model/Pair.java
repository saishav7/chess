package com.chess.model;

/**
 * Created by saishav7 on 1/05/2016.
 */
public class Pair {

    private final Integer left;
    private final Integer right;

    public Pair(Integer left, Integer right) {
        this.left = left;
        this.right = right;
    }

    public int getLeft() { return left; }
    public int getRight() { return right; }

    @Override
    public int hashCode() { return left.hashCode() ^ right.hashCode(); }

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof Pair)) return false;
        Pair pairo = (Pair) o;
        return this.left.equals(pairo.getLeft()) &&
                this.right.equals(pairo.getRight());
    }

    public char getLeftPosition() {
        return (char) (getLeft()+97);
    }

    public int getRightPosition() {
        return (getRight()+1);
    }

    public void printPair() {
        System.out.print(getLeftPosition() + "" + getRightPosition());
    }

}
