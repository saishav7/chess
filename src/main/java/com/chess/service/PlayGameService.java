package com.chess.service;

import com.chess.model.Board;
import com.chess.model.Game;
import com.chess.model.Pair;
import com.chess.model.Player;
import com.chess.model.pieces.King;
import com.chess.model.pieces.Knight;
import com.chess.model.pieces.Pawn;
import com.sun.deploy.util.StringUtils;

import java.util.*;

/**
 * Created by saishav7 on 30/04/2016.
 */
public class PlayGameService {

    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);
        int n;
        String color;
        String type;
        String position;
        boolean continueInput = true;


        while (continueInput) {
            Game game = PlayGameHelper.initializeBoard();
            try {
                System.out.println("");
                System.out.println("Enter number of pieces");
                n = Integer.parseInt(in.nextLine());

                List<Pair> piecePositions = new ArrayList<>();
                for (int i = 0; i < n; i++) {
                    System.out.println();
                    System.out.println("Piece " + (i+1));

                    //Get user input for Player Color
                    System.out.println("Enter color (W/B):");
                    color = in.nextLine();
                    if (!(("W").equals(color) || ("B").equals(color))) {
                        throw new IllegalArgumentException("Invalid Value detected");
                    }

                    //Get user input for Piece Type
                    System.out.println("Enter type (N/P):");
                    type = in.nextLine();
                    if (!(("N").equals(type) || ("P").equals(type))) {
                        throw new IllegalArgumentException("Invalid Value detected");
                    }

                    //Get user input for Piece Position
                    System.out.println("Enter position:");
                    position = in.nextLine();

                    Pair piecePosition = PlayGameHelper.getPiecePosition(position, type);

                    PlayGameHelper.addToBoard(game, color, type, piecePosition);

                    piecePositions.add(piecePosition);
                }

                //init board and show board visual based on user input
                game.initializeBoardGivenPlayers();
                game.getBoard().print();

                PlayGameHelper.calculateAndPrintValidMoves(game, piecePositions);

                System.out.println("Continue? (Y/N)");
                continueInput = ("Y").equals(in.nextLine());

            } catch (IllegalArgumentException e) {
                System.out.println(e.getMessage() + ", try again?");
                continueInput = ("Y").equals(in.nextLine());
            } catch (Exception e) {
                System.out.println("Something went wrong, try again?");
                continueInput = ("Y").equals(in.nextLine());
            }
        }
    }

}
