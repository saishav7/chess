package com.chess.service;

import com.chess.model.*;
import com.chess.model.pieces.King;
import com.chess.model.pieces.Knight;
import com.chess.model.pieces.Pawn;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by saishav7 on 30/04/2016.
 */
public class PlayGameHelper {

    public static Game initializeBoard() {
        Game game = new Game();
        game.setWhite(new Player(true));
        game.setBlack(new Player(false));
        return game;
    }


    public static String addToBoard(Game game, String color, String type, Pair position) {

        Player playerWhite = game.getWhite();
        Player playerBlack = game.getBlack();

        int x = position.getLeft();
        int y = position.getRight();

        if (color.equals("W")) {
            if(type.equals("P")) {
                playerWhite.getPieces().add(new Pawn(true, x, y));
            } else if(type.equals("N")) {
                playerWhite.getPieces().add(new Knight(true, x, y));
            }
        } else if(color.equals("B")) {
            if(type.equals("P")) {
                playerBlack.getPieces().add(new Pawn(false, x, y));
            } else if(type.equals("N")) {
                playerBlack.getPieces().add(new Knight(false, x, y));
            }
        }
        return "";
    }

    public static List<Pair> getValidMoves(Game game, Pair position) {
        Square square = game.getBoard().getSquare(position.getLeft(),position.getRight());
        return findPossibleMoves(game, square.getPiece());
    }

    private static List<Pair> findPossibleMoves(Game game, Piece piece) {
        Pawn p = null;
        Knight n = null;
         List<Pair> moves = new ArrayList<>();
        if(piece.getName() == 'P') {
            p = (Pawn) piece;
        } else if(piece.getName() == 'N') {
            n = (Knight) piece;
        }

        for(int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                Piece currentPiece = null;
                Square square = game.getBoard().getSquare(i,j);
                if(square.isOccupied()) {
                    currentPiece = square.getPiece();
                }
                if(n != null) {
                   if(n.isValid(i,j)) {
                       // filter invalid moves
                       if(currentPiece != null) {
                           // check if square is occupied by same color piece - invalidate move
                           if (currentPiece.isPlayerWhite() == piece.isPlayerWhite()) {
                               continue;
                           }
                       }
                       moves.add(new Pair(i, j));
                   }
                } else if(p != null) {
                    if(p.isValid(i,j)) {
                        // filter invalid moves
                        if(currentPiece != null) {
                            // 1. check if there is a piece blocking the move (e.g. if e2 ahs a piece, a move e1 to e2 is invalid)
                            if(piece.isPlayerWhite()) {
                                if (currentPiece.getX() == piece.getX() && currentPiece.getY() == piece.getY() + 1) {
                                    continue;
                                }
                            } else {
                                if (currentPiece.getX() == piece.getX() && currentPiece.getY() == piece.getY() - 1) {
                                    continue;
                                }
                            }

                            // 2. check if square is occupied by same color piece - invalidate move
                            if (currentPiece.isPlayerWhite() == piece.isPlayerWhite()) {
                                continue;
                            }

                        } else {
                            if(piece.isPlayerWhite()) {
                                // Diagonal move invalid if diagonal square is empty
                                if ((i == piece.getX() + 1 && j == piece.getY() + 1)
                                        || (i == piece.getX() - 1 && j == piece.getY() + 1)) {
                                    continue;
                                }
                            } else {
                                // Diagonal move invalid if diagonal square is empty
                                if ((i == piece.getX() + 1 && j == piece.getY() - 1)
                                        || (i == piece.getX() - 1 && j == piece.getY() - 1)) {
                                    continue;
                                }
                            }
                        }
                        moves.add(new Pair(i, j));
                    }
                }
            }
        }
        return moves;
    }

    public static Pair getPiecePosition(String position, String type) {
        int x = (int) position.charAt(0) - 97;
        int y = Character.getNumericValue(position.charAt(1)) - 1;

        if(x < 0 || x > 7 || y < 0 || y > 7) {
            throw new IllegalArgumentException("Please check position format");
        }

        Pair piecePosition = new Pair(x,y);

        if((piecePosition.getRight() == 0 || piecePosition.getRight() == 7) && type.equals("P")) {
            throw new IllegalArgumentException("Pawns may not be placed on the first or last ranks");
        }
        return piecePosition;
    }

    public static void calculateAndPrintValidMoves(Game game, List<Pair> piecePositions) {
        System.out.println("Valid Moves: -");
        for(Pair piecePosition : piecePositions) {
            List<Pair> validMoves = PlayGameHelper.getValidMoves(game, piecePosition);
            Piece piece = game.getBoard().getSquare(piecePosition.getLeft(), piecePosition.getRight()).getPiece();
            System.out.print((piece.isPlayerWhite()? "White " : "Black ") + piece.getName() +  " On " + piecePosition.getLeftPosition() + "" +  piecePosition.getRightPosition() + " : [");
            for(Pair validMove : validMoves) {
                validMove.printPair();
                if(validMoves.indexOf(validMove) != (validMoves.size()-1)) {
                    System.out.print(", ");
                }
            }
            System.out.println("]");
        }
    }
}
